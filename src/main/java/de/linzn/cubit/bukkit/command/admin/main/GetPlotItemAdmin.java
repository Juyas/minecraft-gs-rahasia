package de.linzn.cubit.bukkit.command.admin.main;

import de.linzn.cubit.bukkit.command.ICommand;
import de.linzn.cubit.bukkit.plugin.CubitBukkitPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * <h1>cubit</h1>
 *
 * @author Julius Korweck
 * @version 13.10.2019
 * @since 13.10.2019
 */
public class GetPlotItemAdmin implements ICommand
{

    private CubitBukkitPlugin plugin;
    private String permNode;

    public GetPlotItemAdmin( CubitBukkitPlugin plugin, String permNode )
    {
        this.plugin = plugin;
        this.permNode = permNode;

    }

    @Override
    public boolean runCmd( Command cmd, CommandSender sender, String[] args )
    {
        if ( !( sender instanceof Player ) )
        {
            /* This is not possible from the server console */
            sender.sendMessage( plugin.getYamlManager().getLanguage().noConsoleMode );
            return true;
        }

        /* Build and get all variables */
        Player player = (Player) sender;

        /* Permission Check */
        if ( !player.hasPermission( this.permNode ) )
        {
            sender.sendMessage( plugin.getYamlManager().getLanguage().errorNoPermission );
            return true;
        }

        if ( !plugin.getVaultManager().givePlotItem( player ) )
        {
            //inventory is full
            sender.sendMessage( plugin.getYamlManager().getLanguage().fullInventory );
            return true;
        }
        else
        {
            sender.sendMessage( plugin.getYamlManager().getLanguage().receivedItem.replace( "{item}", plugin.getYamlManager().getSettings().landItemTitle ) );
            return true;
        }
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2019
 *
 ***********************************************************************************************/