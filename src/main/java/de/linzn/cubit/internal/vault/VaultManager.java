/*
 *  Copyright (C) 2019. MineGaming - All Rights Reserved
 *  You may use, distribute and modify this code under the
 *  terms of the LGPLv3 license, which unfortunately won't be
 *  written for another century.
 *
 *  You should have received a copy of the LGPLv3 license with
 *  this file. If not, please write to: niklas.linz@enigmar.de
 *
 */

package de.linzn.cubit.internal.vault;

import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.WorldGuard;
import de.linzn.cubit.bukkit.plugin.CubitBukkitPlugin;
import de.linzn.cubit.internal.configurations.files.SettingsYaml;
import de.linzn.cubit.internal.vault.eConomy.EconomyHook;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class VaultManager
{

    private CubitBukkitPlugin plugin;
    private Economy econ = null;
    private EconomyHook ecoMrg = null;

    public VaultManager( CubitBukkitPlugin plugin )
    {
        plugin.getLogger().info( "[Setup] VaultManager" );
        this.plugin = plugin;
        if ( setupEconomy() )
        {
            this.ecoMrg = new EconomyHook( plugin, econ );
        }
    }

    private boolean setupEconomy()
    {
        if ( !CubitBukkitPlugin.inst().getYamlManager().getSettings().landEnableEconomy )
        {
            this.plugin.getLogger().info( "Economy is disabled in settings.yml!" );
            return false;
        }
        return hookEconomy( true );
    }

    private boolean hookEconomy( boolean withInfo )
    {
        if ( plugin.getServer().getPluginManager().getPlugin( "Vault" ) == null )
        {
            if ( withInfo )
            {
                this.plugin.getLogger().warning( "Vault not found. Economy is disabled." );
            }
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = plugin.getServer().getServicesManager().getRegistration( Economy.class );
        if ( rsp == null )
        {
            if ( withInfo )
            {
                this.plugin.getLogger().warning( "No valid economy plugin found. Economy is disabled." );
            }
            return false;
        }
        else
        {
            this.plugin.getLogger().info( "Hooked into economy plugin!" );
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public boolean hasEnougToBuy( UUID playerUUID, double value )
    {
        if ( CubitBukkitPlugin.inst().getYamlManager().getSettings().landEnableEconomy && econ == null )
        {
            if ( hookEconomy( false ) )
            {
                this.ecoMrg = new EconomyHook( plugin, econ );
            }
        }

        if ( this.econ != null )
        {
            return this.ecoMrg.hasEnoughToBuy( playerUUID, Math.abs( value ) );
        }
        return true;

    }

    public boolean transferMoney( UUID senderUUID, UUID recieverUUID, double value )
    {
        if ( CubitBukkitPlugin.inst().getYamlManager().getSettings().landEnableEconomy && econ == null )
        {
            if ( hookEconomy( false ) )
            {
                this.ecoMrg = new EconomyHook( plugin, econ );
            }
        }

        if ( this.econ != null )
        {
            try
            {
                ecoMrg.transferMoney( senderUUID, recieverUUID, Math.abs( value ) );
            }
            catch ( Exception e )
            {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public double calculateLandCost( UUID uuid, World world, boolean buyTask )
    {
        if ( CubitBukkitPlugin.inst().getYamlManager().getSettings().landEnableEconomy && econ == null )
        {
            if ( hookEconomy( false ) )
            {
                this.ecoMrg = new EconomyHook( plugin, econ );
            }
        }

        double landBasePrice = CubitBukkitPlugin.inst().getYamlManager().getSettings().landBasePrice;
        double landTaxAddition = CubitBukkitPlugin.inst().getYamlManager().getSettings().landTaxAddition;
        double landMaxPrice = CubitBukkitPlugin.inst().getYamlManager().getSettings().landMaxPrice;
        double landSellPercent = CubitBukkitPlugin.inst().getYamlManager().getSettings().landSellPercent;
        LocalPlayer localplayer = CubitBukkitPlugin.inst().getWorldGuardPlugin()
                .wrapOfflinePlayer( Bukkit.getOfflinePlayer( uuid ) );
        double regionSize = WorldGuard.getInstance().getPlatform().getRegionContainer().get( new BukkitWorld( world ) )
                .getRegionCountOfPlayer( localplayer );
        double price;

        if ( buyTask )
        {
            price = landBasePrice + ( landTaxAddition * regionSize );
            if ( price > landMaxPrice )
            {
                price = landMaxPrice;
            }

        }
        else
        {
            double sellValue = landBasePrice + ( landTaxAddition * regionSize );
            if ( sellValue > landMaxPrice )
            {
                sellValue = landMaxPrice;
            }
            price = ( sellValue * landSellPercent );
        }
        return price;
    }

    public String formattingToEconomy( double value )
    {
        if ( CubitBukkitPlugin.inst().getYamlManager().getSettings().landEnableEconomy && econ == null )
        {
            if ( hookEconomy( false ) )
            {
                this.ecoMrg = new EconomyHook( plugin, econ );
            }
        }
        if ( this.econ != null )
        {
            return ecoMrg.formatToEconomy( value );
        }
        return "0.00";
    }

    //the amount of plot-items the player owns
    private int plotItemAmount( Player player )
    {
        ItemStack plotItem = createPlotItem();
        ItemStack[] contents = player.getInventory().getStorageContents();
        int amount = 0;
        for ( ItemStack item : contents )
        {
            if ( plotItem.isSimilar( item ) )
                amount += item.getAmount();
        }
        return amount;
    }

    //the first slot filled with a plot-item itemstack, or -1 if there is none
    private int firstPlotItemSlot( Player player )
    {
        ItemStack plotItem = createPlotItem();
        ItemStack[] contents = player.getInventory().getStorageContents();
        for ( int i = 0; i < contents.length; i++ )
        {
            ItemStack item = contents[i];
            if ( plotItem.isSimilar( item ) )
                return i;
        }
        return -1;
    }

    /**
     * Creates a new itemstack representing the item to pay for plots.
     * This method does translate the color codes in the title and lore of the item as it can be found in {@link SettingsYaml}
     * using the <code>&</code> symbol as colorcode marker.
     * If any of those landItem fields is not defined correctly, this method will result in an exception, most likely a NullPointerException.
     * This method should not be called if the economy is turned on in the {@link SettingsYaml}, but it does not contain a check for that.
     *
     * @return an itemstack representing the item to buy plots with, amount will be 1
     */
    private ItemStack createPlotItem()
    {
        SettingsYaml settings = CubitBukkitPlugin.inst().getYamlManager().getSettings();
        Material landItemType = settings.landItemType;
        List<String> landItemLore = settings.landItemLore;
        landItemLore = landItemLore.stream().map( elem -> ChatColor.translateAlternateColorCodes( '&', elem ) ).collect( Collectors.toList() );
        String landItemTitle = settings.landItemTitle;
        ItemStack stack = new ItemStack( landItemType );
        ItemMeta itemMeta = stack.getItemMeta();
        itemMeta.setDisplayName( ChatColor.translateAlternateColorCodes( '&', landItemTitle ) );
        itemMeta.setLore( landItemLore );
        stack.setItemMeta( itemMeta );
        stack.setAmount( 1 );
        return stack;
    }

    public boolean givePlotItem( Player player )
    {
        ItemStack plotItem = createPlotItem();
        if ( player.getInventory().firstEmpty() == -1 && plotItemAmount( player ) == 0 )
        {
            return false; //cant add the item, inventory is full
        }
        else
        {
            player.getInventory().addItem( plotItem );
            return true;
        }
    }

    public boolean payPlotItem( Player player )
    {
        ItemStack plotItem = createPlotItem();
        if ( plotItemAmount( player ) == 0 )
            return false;
        int index = firstPlotItemSlot( player );
        ItemStack item = player.getInventory().getItem( index );
        if ( item.getAmount() > 1 )
        {
            //reduce the amount the amount by 1, if the amount is higher than 1 initially
            item.setAmount( item.getAmount() - 1 );
            player.getInventory().setItem( index, item );
        }
        else player.getInventory().remove( plotItem ); //remove if there is only one item
        return true;
    }

}
